* Introduction

This is the companion code/data repository for the paper entitled
*Communication-Aware Load Balancing of the LU Factorization over
Heterogeneous Clusters* by Lucas Leandro Nesi, Lucas Mello Schnorr, and
Arnaud Legrand. The manuscript has been [[https://ieeexplore.ieee.org/document/9359162][accepted]] for publication in
the [[https://icpads2020.comp.polyu.edu.hk/][ICPADS 2020]].

A preprint of the manuscript is available at [[https://hal.inria.fr/hal-02633985][Inria HAL 02633985]].

You can also check the [[./ICPADS.org][ORG source of the manuscript]].

The code snippets presented in this repository are written in R, and
can be used to generate the Figures from the paper. While some Figures
are embedded with no extra manipulation, others are annotated with the
help of external tools to enrich the original paper’s explanation. All
annotations should work seamslessly.

* Requirements

This companion material is written in the R programming language, and
all the scripts make heavy use of the [[https://www.tidyverse.org/][tidyverse]]. In order to reproduce
the work contained in this repository, both environments should be
installed. We also use the [[https://cran.r-project.org/web/packages/patchwork/index.html][patchwork]] and [[https://cran.r-project.org/web/packages/magick/index.html][magick]] for figure
creation. Trace files requires a recent version of the [[https://github.com/schnorr/starvz][starvz]] R
package (with the [[https://cran.r-project.org/web/packages/arrow/index.html][arrow]] package with compression enabled) for
reading/plotting.

The same starvz version can be obtained by:
#+begin_src shell :results output :exports both
git clone https://github.com/schnorr/starvz.git starvz
cd starvz
git checkout icpads2020
#+end_src

And installed with:

#+begin_src R :results output :session *R* :exports both
devtools::install_local("R_package")
#+end_src


This work was tested in the following R environment:

#+BEGIN_SRC R :results output :exports both
library(tidyverse)
library(patchwork)
library(magick)
library(starvz)

sessionInfo()

arrow_available()

arrow::CompressionType
#+END_SRC

#+RESULTS:
#+begin_example
R version 3.6.3 (2020-02-29)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux bullseye/sid

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/openblas-pthread/libblas.so.3
LAPACK: /usr/lib/x86_64-linux-gnu/openblas-pthread/liblapack.so.3

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] starvz_0.1.0.2     arrow_0.17.0.9000  viridis_0.5.1      viridisLite_0.3.0 
 [5] car_3.0-7          carData_3.0-3      gtools_3.8.1       reshape2_1.4.3    
 [9] magrittr_1.5       logging_0.10-108   data.tree_0.7.11   cowplot_1.0.0     
[13] zoo_1.8-6          Rcpp_1.0.4.6       RColorBrewer_1.1-2 pipeR_0.6.1.3     
[17] lpSolve_5.6.15     config_0.3         gridExtra_2.3      magick_2.3        
[21] patchwork_1.0.0    forcats_0.4.0      stringr_1.4.0      dplyr_0.8.3       
[25] purrr_0.3.3        readr_1.3.1        tidyr_1.0.0        tibble_2.1.3      
[29] ggplot2_3.2.1      tidyverse_1.2.1   

loaded via a namespace (and not attached):
 [1] lubridate_1.7.4   lattice_0.20-41   assertthat_0.2.1  zeallot_0.1.0    
 [5] R6_2.4.0          cellranger_1.1.0  plyr_1.8.4        backports_1.1.5  
 [9] httr_1.4.1        pillar_1.4.2      rlang_0.4.5       lazyeval_0.2.2   
[13] curl_4.2          readxl_1.3.1      rstudioapi_0.11   data.table_1.12.8
[17] foreign_0.8-76    bit_1.1-15.2      munsell_0.5.0     broom_0.5.2      
[21] compiler_3.6.3    modelr_0.1.5      pkgconfig_2.0.3   tidyselect_0.2.5 
[25] rio_0.5.16        fansi_0.4.0       crayon_1.3.4      withr_2.1.2      
[29] grid_3.6.3        nlme_3.1-147      jsonlite_1.6.1    gtable_0.3.0     
[33] lifecycle_0.1.0   scales_1.0.0      zip_2.0.4         cli_2.0.2        
[37] stringi_1.4.3     xml2_1.2.2        generics_0.0.2    vctrs_0.2.0      
[41] openxlsx_4.1.4    tools_3.6.3       bit64_0.9-7       glue_1.4.0       
[45] hms_0.5.2         abind_1.4-5       yaml_2.2.0        colorspace_1.4-1 
[49] rvest_0.3.5       haven_2.2.0      
[1] TRUE
$UNCOMPRESSED
[1] 0

$SNAPPY
[1] 1

$GZIP
[1] 2

$BROTLI
[1] 3

$ZSTD
[1] 4

$LZ4
[1] 5

$LZ4_FRAME
[1] 6

$LZO
[1] 7

$BZ2
[1] 8

attr(,"class")
[1] "Compression::type" "arrow-enum"       
#+end_example

* Clone this repository

The [[./traces/]] directory on this repository contains six StarPU traces
whose total size is about 1.4GBytes. These files were uploaded to
Zenodo [[https://doi.org/10.5281/zenodo.7056693][record]] in a compacted [[https://zenodo.org/record/7056694/files/traces.zip?download=1][traces file]].

After the download of the dataset (/traces.zip/) from Zenodo you can
unzip the data:

#+BEGIN_SRC sh
unzip traces.zip
#+END_SRC

* Dataset Description
** Traces

There are six StarPU traces in the manuscript paper, for which we
provide pre-processed ~parquet~ files with the StarVZ framework. The
datasets are detailed in the following table.

| Section | Directory                                | StarPU-SG | Distribution |
|---------+------------------------------------------+-----------+--------------|
| IV      | [[./traces/bc_15_c]]                         | TRUE      | BC           |
| IV      | [[./traces/real_bc_15]]                      | FALSE     | BC           |
| IV      | [[./traces/dist_grid_14che_7chi_1d_1d]]      | TRUE      | 1D1D         |
| IV      | [[./traces/real_dist_grid_14che_7chi_1d_1d]] | FALSE     | 1D1D         |
| V       | [[./traces/paper_dist_100s_21sel_cs]]        | TRUE      | 1D1D+C       |
| V       | [[./traces/paper_dist_100s_21sel_cs_c]]      | TRUE      | 1D1D+C+S     |


* Execution Environment

The experiments conducted on this paper used StarPU 2020 April master
branch, simgrid 3.24, and NewMadeleine master branch on April 2020. Also,
we used Chameleon 0.9.2 with alterations on file =control/descriptor.c=
on function =chameleon_getrankof_2d= to read our distribution file.

*** Real executions
The configuration of the real executions used the following .sh
script to configure each node:
#+begin_src shell :results output :exports both
unset "${!STARPU@}"

export STARPU_SCHED=dmdas
export STARPU_SCHED_BETA=10
export STARPU_MPI_COOP_SENDS=0
export STARPU_FXT_PREFIX="folder_to_save/"

export STARPU_USE_NUMA=1

if [[ $HOSTNAME = chetemi* ]]
then
     export STARPU_RESERVE_NCPU=2
     export STARPU_NCUDA=0
     export STARPU_NCPU=18
     export STARPU_MAIN_THREAD_CPUID="3"
     export STARPU_WORKERS_CPUID="4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38"
     export STARPU_MPI_THREAD_CPUID="0"
fi
if [[ $HOSTNAME = chifflet* ]]
then
    export STARPU_CUDA_PIPELINE=4
    export STARPU_RESERVE_NCPU=2
    export STARPU_NCUDA=2
    export STARPU_NCPU=26
    export STARPU_MAIN_THREAD_CPUID="54"
    export STARPU_MPI_THREAD_CPUID="22"
    export STARPU_WORKERS_CUDAID="1 0"
    export STARPU_WORKERS_CPUID="28 30 32 34 36 38 40 42 44 46 48 50 0 2 4 6 8 10 12 14 16 18 20 24 26 52 54"
fi
#+end_src

The execution command:

#+begin_src shell :results output :exports both
source ~/node_sh.sh  && ./mpirun --np 21 --hostfile $HOSTFILE ./timing/time_dgetrf_nopiv_tile --nb=960 --n_range=96000:96000:96000 --gpus=2 --trace --nowarmup --P=1
#+end_src

*** Simulation executions
The simulations used performance models of the real executions.

The simulation command:

#+begin_src shell :results output :exports both
./starpu_smpirun -platform $CLUSTER_FILE -hostfile $HOSTFILE ./timing/time_dgetrf_nopiv_tile -b 960 -N 96000:96000:96000 -g 2 -T -w -P 1
#+end_src

* Figures

| Figure | Script    | Image                                    |
|--------+-----------+------------------------------------------|
|      1 | NA        | NA (see tikz code)                       |
|      2 | NA        | [[./imgs/partitions.pdf]]                    |
|      3 | [[./fig3.R]]  | [[./imgs/sec5/1d1d_21_pure.pdf]]             |
|      4 | [[./fig4.sh]] | [[./imgs/LU_DAG.pdf]]                        |
|      5 | [[./fig5.R]]  | [[./imgs/sec5/bc_15_real_vs_simu.pdf]]       |
|      6 | [[./fig6.R]]  | [[./imgs/sec5/1d1d_21_real_vs_simu.pdf]]     |
|      7 | [[./fig7.R]]  | [[./imgs/sec_5_1d_1dc_info.pdf]]             |
|      8 | [[./fig8.R]]  | [[./imgs/sec5/merged_1d1d_21_cs_vs_sf.pdf]]  |
|      9 | [[./fig9.R]]  | [[./imgs/sec_5_cumulative_abe_combined.pdf]] |
|     10 | [[./fig10.R]] | [[./imgs/sec_6_homogeneous.pdf]]             |
|     11 | [[./fig11.R]] | [[./imgs/sec_6_strong_scale_com.pdf]]        |
|     12 | [[./fig13.R]] | [[./imgs/sec_6_matrix_size_new.pdf]]         |

Regenerate all figures:

#+begin_src shell :results output
Rscript fig3.R
source fig4.sh
for script in fig5.R fig6.R fig7.R fig8.R fig9.R fig10.R fig11.R fig12.R; do
  Rscript $script
done
#+end_src


