#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  int i;
  int j;
} ij_t;

// a matrix of last operations by coordinate
int *last_tids = NULL;
int cols = 0;
int rows = 0;

void declare_node (int tid, const char *operation, ij_t c) {
  static int max_tid = -1;
  if (tid > max_tid) {
    //color
    char *color = NULL;
    if (strstr(operation, "DGTRF") != NULL) {
      color = "#e41a1c";
    }else if (strstr(operation, "DTRSM") != NULL) {
      color = "#377eb8";
    }else if (strstr(operation, "DGEMM") != NULL) {
      color = "#4daf4a";
    }else{
      color = "white";
    }
    printf ("tid_%d [label=\"%d,%d\", "
	    "style=\"filled\", "
	    "shape=\"circle\", "
	    "color=\"%s\"];\n",
	    tid,
	    c.i, c.j,
	    color);
    max_tid = tid;
  }
}

void node(int tid, const char *operation, const char *m, ij_t c) {
  int offset = c.i * cols + c.j;
  int last_tid = last_tids[offset];
  if (last_tid != -1){
    printf("tid_%d -> tid_%d;\n", last_tid, tid);
  }
  if (strstr(m, "RW") != NULL){
    declare_node (tid, operation, c);
    last_tids[offset] = tid;
  }
}

void DGTRF (int tid, const char *m, ij_t c) {
  node(tid, __func__, m, c);
}

void DTRSM (int tid, const char *m1, ij_t c1, const char *m2, ij_t c2) {
  node(tid, __func__, m2, c2);
  node(tid, __func__, m1, c1);
}

void DGEMM (int tid, const char *m1, ij_t c1, const char *m2, ij_t c2, const char *m3, ij_t c3) {
  node(tid, __func__, m3, c3);
  node(tid, __func__, m2, c2);
  node(tid, __func__, m1, c1);
}

int main(int argc, char **argv) {
  int N = argc==2?atoi(argv[1]):3;
  rows = cols = N;
  last_tids = (int*)malloc(rows*cols*sizeof(int));
  bzero(last_tids, rows*cols*sizeof(int));
  for(int i; i < rows*cols; i++) last_tids[i] = -1;
  printf("digraph lu {\n"
	 "rankdir=\"LR\";\n");
  int tid = 0;
  for (int k = 0; k < N; k++) {
    ij_t kk = {k, k};
    DGTRF(tid++, "RW", kk);
    for (int m = k+1; m < N; m++) {
      ij_t mk = {m, k};
      ij_t km = {k, m};
      DTRSM(tid++, "RW", mk, "R", kk);
      DTRSM(tid++, "RW", km, "R", kk);
    }
    for (int n = k+1; n < N; n++) {
      for (int m = k+1; m < N; m++) {
	ij_t mn = {m, n};
	ij_t mk = {m, k};
	ij_t kn = {k, n};
	DGEMM(tid++, "RW", mn, "R", mk, "R", kn);
      }
    }
  }
  printf("subgraph cluster_01{\n"
  "nodesep = 1;\n"
  "ranksep = 2;\n"
  "pad=\"0.5\";\n"
  "style = invis;\n"
  "edge [style=invis];\n"
  "dgetrf_nopiv [style=filled, fillcolor=\"#e41a1c\", shape=rectangle];\n"
  "dtrsm [style=filled, fillcolor=\"#377eb8\", shape=rectangle];\n"
  "dgemm [style=filled, fillcolor=\"#4daf4a\", shape=rectangle];\n"
  "dgetrf_nopiv -> dtrsm;\n"
  "dtrsm -> dgemm;\n"
  "}\n");
  printf("}\n");
  free(last_tids);
  return 0;
}
