#include <Rcpp.h>
#include <vector>
using namespace Rcpp;

// [[Rcpp::export]]
DataFrame lu_dag_c(int blocks) {
  int total_pot = blocks;
  int total_sm = (blocks * blocks) - blocks;
  int si = blocks - 1;
  int total_gem = (si * (si + 1) * (2*si+1) )/6; //This is equal to the sum i^2 from i to n where n is blocks - 1
  int total_lines = total_pot + total_sm * 2 + total_gem * 3;
  
  IntegerVector JobId(total_lines);
  IntegerVector Dependent(total_lines);
  IntegerVector x(total_lines);
  IntegerVector y(total_lines);
  IntegerVector xd(total_lines);
  IntegerVector yd(total_lines);
  CharacterVector Value(total_lines);
  IntegerVector Ite(total_lines);

  std::vector<int> PM(blocks * blocks);

   int lin = 0;
   int tid = 0;
    
   for(int k=1; k<=blocks; k++){
       tid++;

       JobId[lin] = tid;
       Dependent[lin] = PM[(k-1) * blocks + (k-1)];
       x[lin] = k;
       y[lin] = k;
       xd[lin] = k;
       yd[lin] = k;
       Value[lin] = "dgetrf_nopiv";
       Ite[lin] = k;
       lin++;

       PM[(k-1) * blocks + (k-1)] = tid;

       if(k < blocks){
          for(int m=k+1; m<=blocks; m++){
            tid++;

            JobId[lin] = tid;
            Dependent[lin] = PM[(k-1) * blocks + (k-1)];
            x[lin] = m;
            y[lin] = k;
            xd[lin] = k;
            yd[lin] = k;
            Value[lin] = "dtrsm";
            Ite[lin] = k;
            lin++;

            JobId[lin] = tid;
            Dependent[lin] = PM[(m-1) * blocks + (k-1)];
            x[lin] = m;
            y[lin] = k;
            xd[lin] = m;
            yd[lin] = k;
            Value[lin] = "dtrsm";
            Ite[lin] = k;
            lin++;

            PM[(m-1) * blocks + (k-1)] = tid;

          }

          for(int n=k+1; n<=blocks; n++){
            tid++;

            JobId[lin] = tid;
            Dependent[lin] = PM[(k-1) * blocks + (k-1)];
            x[lin] = k;
            y[lin] = n;
            xd[lin] = k;
            yd[lin] = k;
            Value[lin] = "dtrsm";
            Ite[lin] = k;
            lin++;

            JobId[lin] = tid;
            Dependent[lin] = PM[(k-1) * blocks + (n-1)];
            x[lin] = k;
            y[lin] = n;
            xd[lin] = k;
            yd[lin] = n;
            Value[lin] = "dtrsm";
            Ite[lin] = k;
            lin++;

            PM[(k-1) * blocks + (n-1)] = tid;

            for(int m=k+1; m<=blocks; m++){
              tid++;

              JobId[lin] = tid;
              Dependent[lin] = PM[(m-1) * blocks + (k-1)];
              x[lin] = m;
              y[lin] = n;
              xd[lin] = m;
              yd[lin] = k;
              Value[lin] = "dgemm";
              Ite[lin] = k;
              lin++;

              JobId[lin] = tid;
              Dependent[lin] = PM[(k-1) * blocks + (n-1)];
              x[lin] = m;
              y[lin] = n;
              xd[lin] = k;
              yd[lin] = n;
              Value[lin] = "dgemm";
              Ite[lin] = k;
              lin++;

              JobId[lin] = tid;
              Dependent[lin] = PM[(m-1) * blocks + (n-1)];
              x[lin] = m;
              y[lin] = n;
              xd[lin] = m;
              yd[lin] = n;
              Value[lin] = "dgemm";
              Ite[lin] = k;
              lin++;

              PM[(m-1) * blocks + (n-1)] = tid;

            }

          }        
       }
   }


  DataFrame df = DataFrame::create( Named("JobId") = JobId,
                                    _["Dependent"] = Dependent,
                                    _["x"] = x,
                                    _["y"] = y,
                                    _["xd"] = xd,
                                    _["yd"] = yd,
                                    _["Value"] = Value,
                                    _["Ite"] = Ite );

  return df;
}
