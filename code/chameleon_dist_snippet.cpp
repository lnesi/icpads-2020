#include <stdio.h>

typedef struct custom_dist_s{
  int nstart;
  int *blocks_dist;
  int dist_m, dist_n;
  const char* dist_file;
} custom_dist_t;

/**
 * @brief Function to allocate the dist
 */
inline static void alloc_dist(custom_dist_t* custom_dist, int m, int n){
  custom_dist->blocks_dist = (int*)malloc(sizeof(int) * m * n);
  if(custom_dist->blocks_dist==NULL){
      printf("[Dist %s] Error allocation\n", custom_dist->dist_file);
  }
}

/**
 * @brief Function to load the dist file specified by custom_dist->dist_file
 * The dist file is compound of: First line M N followed by M lines with N integers (with values from 1 to num of nodes)
 */
inline static void load_dist(custom_dist_t* custom_dist, const CHAM_desc_t *A)
{
  custom_dist->nstart=1;
  FILE * f = fopen ( custom_dist->dist_file , "r" );
  if(f==NULL){
      printf("[Dist %s] No dist file found\n", custom_dist->dist_file);
      custom_dist->nstart=-1;
      return;
  }

  int m, n;
  fscanf(f, "%d %d", &m, &n);
  if(A){
    if(m!=A->lmt || n!=A->lnt){
      printf("[Dist %s] Invalid dist sizes Matrix: %d %d - Dist: %d %d\n", custom_dist->dist_file, A->lmt, A->lnt, m, n);
      exit(1);
    }
  }

  custom_dist->dist_m = m;
  custom_dist->dist_n = n;

  alloc_dist(custom_dist, m, n);

  for(int i=0; i < m; i++){
      for(int j=0; j < n; j++){
          fscanf(f, "%d", &custom_dist->blocks_dist[i * n + j]);
          custom_dist->blocks_dist[i * n + j]-=1;
      }
  }
  fclose(f);
}

const char *dist_a = "dist";
static custom_dist_t custom_dist_a = {0, NULL, 0, 0, dist_a};

/**
 * @brief Internal function to return MPI rank of block (m,n) in distribution
 * custom from dist file
 * @param[in] A matrix
 * @param[in] m row index of tile to consider
 * @param[in] n column index of tile to consider
 */
static int chameleon_getrankof_custom( const CHAM_desc_t *A, int m, int n )
{
  if(custom_dist_a.nstart==0){
      load_dist(&custom_dist_a, A);
  }
  int res = -1;
  if(custom_dist_a.nstart==1){
    res = custom_dist_a.blocks_dist[m * custom_dist_a.dist_n + n];
    if(res < 0 || res > 100000){ // Quick check for corrupted memory
      printf("[Dist A] Some wrong return: m:%d, n:%d\n", m, n);
      exit(1);
    }
  }
  return res;
}

static int chameleon_getrankof_2d( const CHAM_desc_t *A, int m, int n ){
	return chameleon_getrankof_custom(A, m, n);
}
