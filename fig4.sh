pushd code
export RACINE=lu_to_dag
gcc $RACINE.c -o $RACINE
export N=3
mkdir -p ../imgs/
./$RACINE $N | dot -Tpng -o ../imgs/LU_DAG.png
rm -f $RACINE
unset RACINE
unset N
popd
