for (k = 0; k < N; k++)
  |\color{dpotrfcolor}\textbf{DGTRF-NOPIV}|(|\underline{RW}|, A[k][k]);
  for (m = k+1; m < N; m++)
    |\color{dtrsmcolor}\textbf{DTRSM}|(|\underline{RW}|, A[m][k], |\underline{R}|, A[k][k]);
    |\color{dtrsmcolor}\textbf{DTRSM}|(|\underline{RW}|, A[k][m], |\underline{R}|, A[k][k]);
  for (n = k+1; n < N; n++) |\textbf{// Update}|
    for (m = k+1; m < N; m++)
      |\color{dgemmcolor}\textbf{DGEMM}|(|\underline{RW}|, A[m][n], |\underline{R}|, A[m][k],
                         |\underline{R}|, A[k][n]);
